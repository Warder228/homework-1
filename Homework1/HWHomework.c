//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <time.h>

const char* ZERO = "zero";
const char* ONE = "one";
const char* TWO = "two";
const char* THREE = "three";
const char* FOUR = "four";
const char* FIVE = "five";
const char* SIX = "six";
const char* SEVEN = "seven";
const char* EIGHT = "eight";
const char* NINE = "nine";
const char* DOT = "dot";
const char* COMMA = "comma";

//размер
const int SIZE_ZERO = 4;
const int SIZE_ONE = 3;
const int SIZE_TWO = 3;
const int SIZE_THREE = 5;
const int SIZE_FOUR = 4;
const int SIZE_FIVE = 4;
const int SIZE_SIX = 3;
const int SIZE_SEVEN = 5;
const int SIZE_EIGHT = 5;
const int SIZE_NINE = 4;
const int SIZE_DOT = 3;
const int SIZE_COMMA = 5;

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    srand(time(NULL));
    long ** arrayPtr = (long**) calloc(rows,sizeof(long));
    for(int i = 0; i < rows; ++i)
    {
       arrayPtr[i] = (long*) calloc(columns, sizeof(long));
       for(int j = 0; j < columns; ++j)
        {
            arrayPtr[i][j] = 1 + rand() % 100; //ограничим значения в диапазоне 100
        }
    }
    
    
    return arrayPtr;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    
    int stringSize = 0;
    int numbersSize = 1;
    while (numberString[stringSize] != '\0')
    {
        if (numberString[stringSize] == ' ')
        {
            ++numbersSize;
        }
        ++stringSize;
    }
    
    char** arrayStrings = (char**) calloc(numbersSize, sizeof(char*));
    int TekSizeWord = 0;
    int TekWord = 0;
    int ResultStringSize = 0;
    
    for (int i = 0; i <= stringSize; ++i)//считываем  и забиваем в массив чисел (предположительно чисел более чем 2)
    {
        if ((numberString[i] == ' ') || (numberString[i] == '\0'))
        {
            int counter = 0;
            arrayStrings[TekWord] = (char*) calloc(TekSizeWord, sizeof(char));
            for(int j = i - TekSizeWord; j < i; ++j)
            {
                //подсчет размера  слова
                switch (numberString[j]) {              
                    case '0':
                        ResultStringSize += SIZE_ZERO;
                        break;
                    case '1':
                        ResultStringSize += SIZE_ONE;
                        break;
                    case '2':
                        ResultStringSize += SIZE_TWO;
                        break;
                    case '3':
                        ResultStringSize += SIZE_THREE;
                        break;
                    case '4':
                        ResultStringSize += SIZE_FOUR;
                        break;
                    case '5':
                        ResultStringSize += SIZE_FIVE;
                        break;
                    case '6':
                        ResultStringSize += SIZE_SIX;
                        break;
                    case '7':
                        ResultStringSize += SIZE_SEVEN;
                        break;
                    case '8':
                        ResultStringSize += SIZE_EIGHT;
                        break;
                    case '9':
                        ResultStringSize += SIZE_NINE;
                        break;
                    case '.':
                        ResultStringSize += SIZE_DOT;
                        break;
                    case ',':
                        ResultStringSize += SIZE_COMMA;
                        break;
                    default:
                        break;
                }//endSwitch
                ++ResultStringSize;
                arrayStrings[TekWord][counter] = numberString[j];
                ++counter;
            }//endFor
            
            ++TekWord;
            TekSizeWord = 0;
        }//endIf
        
        ++TekSizeWord; //в размер слова пробел не входит
    }//endFor
    
    --ResultStringSize;//удаляем лишний пробел
    TekWord = 0;
    
    char* WordString = (char*) calloc(ResultStringSize, sizeof(char));
    int PositionInNow = 0;
    for(int i = 0; i < numbersSize; ++i)
    {
        int counter = 0;
        while (arrayStrings[TekWord][counter] != '\0')
        {
            switch (arrayStrings[TekWord][counter]) {              //парсер?
                case '0':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_ZERO; ++k)
                    {
                        WordString[k] = ZERO[k - PositionInNow];
                    }
                    PositionInNow += SIZE_ZERO;
                    break;
                case '1':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_ONE; ++k)
                    {
                        WordString[k] = ONE[k - PositionInNow];
                    }
                    PositionInNow += SIZE_ONE;
                    break;
                case '2':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_TWO; ++k)
                    {
                        WordString[k] = TWO[k - PositionInNow];
                    }
                    PositionInNow += SIZE_TWO;
                    break;
                case '3':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_THREE; ++k)
                    {
                        WordString[k] = THREE[k - PositionInNow];
                    }
                    PositionInNow += SIZE_THREE;
                    break;
                case '4':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_FOUR; ++k)
                    {
                        WordString[k] = FOUR[k - PositionInNow];
                    }
                    PositionInNow += SIZE_FOUR;
                    break;
                case '5':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_FIVE; ++k)
                    {
                        WordString[k] = FIVE[k - PositionInNow];
                    }
                    PositionInNow += SIZE_FIVE;
                    break;
                case '6':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_SIX; ++k)
                    {
                        WordString[k] = SIX[k - PositionInNow];
                    }
                    PositionInNow += SIZE_SIX;
                    break;
                case '7':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_SEVEN; ++k)
                    {
                        WordString[k] = SEVEN[k - PositionInNow];
                    }
                    PositionInNow += SIZE_SEVEN;
                    break;
                case '8':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_EIGHT; ++k)
                    {
                        WordString[k] = EIGHT[k - PositionInNow];
                    }
                    PositionInNow += SIZE_EIGHT;
                    break;
                case '9':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_NINE; ++k)
                    {
                        WordString[k] = NINE[k - PositionInNow];
                    }
                    PositionInNow += SIZE_NINE;
                    break;
                case '.':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_DOT; ++k)
                    {
                        WordString[k] = DOT[k - PositionInNow];
                    }
                    PositionInNow += SIZE_DOT;
                    break;
                case ',':
                    for(int k = PositionInNow; k < PositionInNow + SIZE_COMMA; ++k)
                    {
                        WordString[k] = COMMA[k - PositionInNow];
                    }
                    PositionInNow += SIZE_COMMA;
                    break;
                default:
                    break;
            }//endSwitch
            
            if (arrayStrings[TekWord][counter + 1] != '\0')
            {
                WordString[PositionInNow] = ' ';
                ++PositionInNow;
            }
            ++counter;
        }//endWhile
        
        ++TekWord;
        if (TekWord != numbersSize) //если слово не последнее
        {
            WordString[PositionInNow] = ',';
            WordString[PositionInNow + 1] = ' ';
            PositionInNow += 2;
        }
        
    }//endFor    
    return WordString;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    //нормальный программист умеет в математику ...я умею так
    double x, y;
    
    x = fmod(time * 16 , canvasSize);
    y = (sin(x / 16) * (canvasSize / 2)) + canvasSize / 2;
    
    return (HWPoint){x, y};
}

